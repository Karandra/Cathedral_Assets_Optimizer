<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>Will write which files would be affected, without actually processing them</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Open Directory</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>Run</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>One mod</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>Dry run</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>Show advanced settings</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>Extract BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>Delete backups </translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>Create BSA</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>Morrowind</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>Oblivion</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>Skyrim / Fallout 3 / Fallout New Vegas</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>Fallout 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>Maximum size</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>Extension</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>Suffix</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>Meshes</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>Necessary optimization</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>Medium optimization</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>Full optimization</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>Always process headparts</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>Resave meshes</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>Oblivion (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>User</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>Oblivion (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>Stream</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>Oblivion / Fallout 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>Skyrim (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>Skyrim SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>Fallout 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>Textures</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>Resizing</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Height:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Width:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>By ratio</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>By fixed size</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>Enable TGA conversion</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profile</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>New profile</translation>
    </message>
    <message>
        <source>New</source>
        <translation>New</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>Several mods </translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>Process BSAs</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>Create textures BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>Expert</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>BSA Format</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>Textures BSA Format</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>Process meshes</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>Process textures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>Compress textures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>Generate mipmaps</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>Output format</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>Uncompressed (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>Process animations</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>Enable debug log</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>Used when reporting bugs</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>Discord</translation>
    </message>
    <message>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>About Qt</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>Show tutorials</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>Open log file</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>Compress interface textures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>Unwanted formats</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>Animations</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>Tools</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>Enable dark theme</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>Advanced settings</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>Advanced settings can only be modified when using custom profiles.</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>Several mods option</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>This process has only been tested on the Mod Organizer mods folder.</translation>
    </message>
    <message>
        <source>
Made by G&apos;k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>
Made by G&apos;k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>Save unsaved changes</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>Base profile</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>Which profile do you want to use as a base?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation>An exception has been encountered and the process was forced to stop: </translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Done</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>Welcome to %1 %2</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Search</translation>
    </message>
</context>
</TS>
